const { Given, When, Then } = require('@cucumber/cucumber');
const ShowroomBusinessPage = require('../pageobjects/showroom.business.page');
const assert = require('assert')

const pages = {
    "business vehicles": ShowroomBusinessPage
}

Given(/^I am on the ([\w0-9 -]+) page$/, page => {
    pages[page].open();
});

/************ Business Vehicle Steps ************/

// Generic set-a-filter just to enable filtered results

When(/^I set a filter$/, () => {
    ShowroomBusinessPage.setQuickFilter("SUV");
});

// Quick filters and make search

When(/^I click on the quick filter ([\w0-9 -]+)$/, quickFilter => {
    ShowroomBusinessPage.setQuickFilter(quickFilter);
});

When(/^I search for ([\w0-9 -]+) in the make search bar/, searchTerm => {
    ShowroomBusinessPage.vehicleMakeSearchBarInput.setValue(searchTerm);
    ShowroomBusinessPage.vehicleMakeSearchBarButton.click();
});

Then(/^the ([\w0-9 -]+) filter should be applied$/, filter => {
    filterButton = ShowroomBusinessPage.findAppliedFilterButton(filter);
    filterButton.click();
    let checkbox = $(`#${filter}`);
    expect(checkbox).toBeExisting();
    expect(checkbox).toBeSelected();
});

Then(/^display a "0 results" error$/, () => {
    expect(ShowroomBusinessPage.vehicleMakeSearchError).toBeExisting();
});

// Make and Model filters

When(/^I filter make ([\w0-9 -]+) and model ([\[\]\w0-9 -]+)$/, (make, model) => {
    ShowroomBusinessPage.setMakeModelFilter(make, model);
});

When(/^I search for ([\w0-9 -]+) in the Make and Model menu$/, searchTerm => {
    ShowroomBusinessPage.filterMakeModelButton.click();
    ShowroomBusinessPage.makeModelSearchBarInput.setValue(searchTerm);
});

When(/^I should see ([\]\[\w0-9 -]+) in the Make and Model menu$/, make => {
    if (make != "[none]") {
        expect(ShowroomBusinessPage.makeModelFilterMenu().$(`span=${make}`)).toBeExisting();
    } else {
        expect(ShowroomBusinessPage.makeModelFilterMenu().$(`span=${make}`)).not.toBeExisting();
    }
});

Then(/^I should see only ([\w0-9 -]+) makes and ([\[\]\w0-9 -]+) models$/, (make, model) => {
    let makeModelCount = ShowroomBusinessPage.makeModelOffersCount(make, model);
    expect(ShowroomBusinessPage.allOffers).toBeElementsArrayOfSize({ eq: makeModelCount });
});

// Monthly price filters 

When(/^I set the monthly price sliders between ([0-9]+) and ([0-9]+)$/, (min, max) => {
    ShowroomBusinessPage.setMonthlyPriceFilter(min, max);
});

Then(/^I should see only results in my price range$/, () => {
    let range = ShowroomBusinessPage.monthlyPriceMinMax;
    // Note: sometimes there's multiple matches for LocalizedPrice, as the 
    //       crossed out menu showing an old price would trigger this. to
    //       get around it, I added the Heading selector before it.
    let offerPrices = ShowroomBusinessPage.carOfferings.$$('span[data-component="Heading"]>[data-component="LocalizedPrice"]');
    for (let i = 0; i < offerPrices.length; i++) {
        // Remove all non-number characters from each localized price.
        let price = parseInt(offerPrices[i].getText().replace(/[^0-9]/g, ""));
        assert.ok(range.min <= price <= range.max, "Price out of set range!");
    }
});

// Popular filters

When(/^I set the ([\w0-9 -]+) popular filter$/, filter => {
    ShowroomBusinessPage.setPopularFilter(filter);
});

Then(/^I should see only Best deals results$/, () => {
    let offerCount = ShowroomBusinessPage.totalOfferCount;
    expect(ShowroomBusinessPage.bestDeals).toBeElementsArrayOfSize({ eq: offerCount });
});

Then(/^I should see only No stress plan results$/, () => {
    let offerCount = ShowroomBusinessPage.totalOfferCount;
    expect(ShowroomBusinessPage.noStressPlans).toBeElementsArrayOfSize({ eq: offerCount });
});

Then(/^I should see only Configure yourself results$/, () => {
    expect(ShowroomBusinessPage.noStressPlans).toBeElementsArrayOfSize({ eq: 0 });
    expect(ShowroomBusinessPage.bestDeals).toBeElementsArrayOfSize({ eq: 0 });
});

// Sorting options

When(/^I sort from price low to high$/, () => {
    ShowroomBusinessPage.sortByInput.selectByAttribute('data-key', 'features.showroom.sorting.priceAsc');
    browser.pause(1000);
});

When(/^I sort from price high to low$/, () => {
    ShowroomBusinessPage.sortByInput.selectByAttribute('data-key', 'features.showroom.sorting.priceDesc');
    browser.pause(1000);
});

Then(/^the lowest prices are displayed first$/, () => {
    // Since Best Deals are made to show up first regardless of price, allow
    // for one out-of-order price which is used to detect the transition between
    // best deals and regular listings. 
    let offerPrices = ShowroomBusinessPage.carOfferings.$$('span[data-component="Heading"]>[data-component="LocalizedPrice"]');
    let lastPrice = null;
    let finishedBestDeals = false;
    for (let i = 0; i < offerPrices.length; i++) {
        // Strip non-number characters from price
        let price = parseInt(offerPrices[i].getText().replace(/[^0-9]/g, ""));
        // If we haven't compared prices yet, save the value and continue
        if (lastPrice == null) {
            lastPrice = price;
            continue;
        }
        // As long as the price is increasing, continue like normal
        if (price >= lastPrice) {
            lastPrice = price;
            continue;
        } else {
            // If this is the first time we've gotten an out-of-order
            // price, then assume we've finished looking at best deals
            // and continue on.
            if (!finishedBestDeals) {
                finishedBestDeals = true;
                lastPrice = price;
                continue;
            } else {
                // However, if it's the 2nd time, fail the test case.
                assert.ok(false, "Price not in ascending order!");
                lastPrice = price;
            }
        }
    }
});

Then(/^the highest prices are displayed first$/, () => {
    // Since Best Deals are made to show up first regardless of price, allow
    // for one out-of-order price which is used to detect the transition between
    // best deals and regular listings. 
    let offerPrices = ShowroomBusinessPage.carOfferings.$$('span[data-component="Heading"]>[data-component="LocalizedPrice"]');
    let lastPrice = null;
    let finishedBestDeals = false;
    for (let i = 0; i < offerPrices.length; i++) {
        // Strip non-number characters from price
        let price = parseInt(offerPrices[i].getText().replace(/[^0-9]/g, ""));
        // If we haven't compared prices yet, save the value and continue
        if (lastPrice == null) {
            lastPrice = price;
            continue;
        }
        // As long as the price is decreasing, continue like normal
        if (price <= lastPrice) {
            lastPrice = price;
            continue;
        } else {
            // If this is the first time we've gotten an out-of-order
            // price, then assume we've finished looking at best deals
            // and continue on.
            if (!finishedBestDeals) {
                finishedBestDeals = true;
                lastPrice = price;
                continue;
            } else {
                // However, if it's the 2nd time, fail the test case.
                assert.ok(false, "Price not in decending order!");
                lastPrice = price;
            }
        }
    }
});

// Deleting filters

When(/^I click the delete filters button$/, () => {
    ShowroomBusinessPage.filterResetFiltersButton.click();
});

Then(/^all filters should be removed$/, () => {
    expect($('[data-e2e-id="No Stress Plan Grid"]')).toBeExisting();
});