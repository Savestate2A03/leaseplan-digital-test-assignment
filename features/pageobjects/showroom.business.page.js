const Page = require('./page');

const MENU_TIMEOUT = 1000;
/**
 * sub page containing specific selectors and methods for a specific page
 */

// Workaround for WDIO not respecting certain clicks
const clickInBrowser = e => { e.click() };

class ShowroomBusinessPage extends Page {

    /* Quick filters, currently:
        * Electric
        * SUV
        * Automatic
        * Hybrid
        * Petrol
        * Commercial Vehicle (redirect) */
    get quickFilters () { return $('[data-component="QuickFilters"]') }

    setQuickFilter (filter) {
        this.quickFilters.$(`[data-key="${filter}"]`).click();
    }

    /* Primary filters users can apply */

    // Traverse upwards until a filter's button has been found,
    // otherwise return the original element if no button found.
    findFilterButton (selector) {
        let e;

        // No support for mobile implemented yet, however it wouldn't 
        // take too much time I'd assume. Much of the layout is shared.
        if ($('[data-component="MobileFilters"]').isDisplayed()) {
            e = $('[data-component="MobileFilters"]').$(selector);
        } else {
            e = $('[data-component="desktop-filters"]').$(selector);
        }

        // If the filter doesn't exist, it lies in the More Filters button.
        if (!e.isExisting()) {
            e = $('[data-key="moreFilters"]');
        } 

        // If the button or body element is found, abort.
        while(e.getTagName() != "body" && e.getTagName() != "button") {
            e = e.$('..');
        }

        if (e.getTagName() == "button") {
            return e;
        }

        throw new Error(`Filter ${filter} not found!`);
    }

    // Primary filters
    get filterPopularButton () { return this.findFilterButton('[data-key="popularFilters"]') }
    get filterMakeModelButton () { return this.findFilterButton('[data-key="makemodel"]') }
    get filterMonthlyPriceButton () { return this.findFilterButton('[data-key="monthlyPrice"]') }
    get filterLeaseOptionsButton () { return this.findFilterButton('[data-key="leaseOption"]') }
    get filterFuelTypeButton () { return this.findFilterButton('[data-key="fuelTypes"]') }
    get filterBodyTypeButton () { return this.findFilterButton('[data-key="bodyTypes"]') }
    get filterTransmissionButton () { return this.findFilterButton('[data-key="transmissions"]') }
    get filterResetFiltersButton () { return this.findFilterButton('[data-key="resetFilters"]') }

    clickOpenFilterSaveButton () {
        $('[data-key="features.showroom.filters.save"]').$('..').click();
    }

    findAppliedFilterButton (appliedFilter) {
        // appliedFilter is the id of an input for a specific filter
        const filterBase = appliedFilter.match(/(\w+)-\w+/);
        if (filterBase != null) {
            appliedFilter = filterBase[1];
        }

        switch(appliedFilter) {
            case 'Best deals':
            case 'Configure yourself':
            case 'No stress plan':
                return this.filterPopularButton;
            case 'make':
            case 'model':
                return this.filterMakeModelButton;
            case 'fuelType':
                return this.filterFuelTypeButton;
            case 'bodyType':
                return this.filterBodyTypeButton;
            case 'transmission':
                return this.filterTransmissionButton;
            default:
        }
        throw new Error(`Unable to find applied filter "${appliedFilter}"!`);
    }

    /* Primary filter setters */

    setPopularFilter (filter) {
        this.filterPopularButton.click();

        // Click on the "Popular filters" button
        let popularFiltersMenu = $('[data-component="CheckboxFilter"][data-key="popularFilters"]');
        popularFiltersMenu.waitForExist({ timeout: MENU_TIMEOUT });
        popularFiltersMenu.$(`[data-e2e-id="${filter}"]`).$('label').click();

        this.clickOpenFilterSaveButton();
    }

    setMakeModelFilter (make, model) {
        this.filterMakeModelButton.click();

        // Click on the "Make and Model" button
        let makeModelFilterMenu = $('[data-component="MakeModelFilter"]');
        makeModelFilterMenu.waitForExist({ timeout: MENU_TIMEOUT });

        // Search for the inputs matching the provided make and model
        let makeInput = $(`#make-${make.toUpperCase()}`).$('..');
        makeInput.waitForExist({ timeout: MENU_TIMEOUT });
        makeInput.click();

        if (model != "[none]") {
            $(`#model-${model.toUpperCase()}`).$('..').click();
        }

        this.clickOpenFilterSaveButton();
    }

    setMonthlyPriceFilter (min, max) {
        // This function physically slides the bars in the 
        // price range tool. However, it can only approximate
        // the value of where the slider will go. This is fine
        // however, as you can adjust test cases to respect the
        // reported price range. 

        this.filterMonthlyPriceButton.click();
        let priceFilter = $('[data-component="PriceFilter"]');
        priceFilter.waitForExist({ timeout: MENU_TIMEOUT });

        let rcSlider = priceFilter.$('.rc-slider');
        let rcSliderWidth = rcSlider.getSize('width');
        let rcSliderMinHandle = rcSlider.$('.rc-slider-handle-1');
        let rcSliderMaxHandle = rcSlider.$('.rc-slider-handle-2');

        let rcSliderMinValue = parseInt(rcSliderMinHandle.getAttribute('aria-valuemin'));
        let rcSliderMaxValue = parseInt(rcSliderMaxHandle.getAttribute('aria-valuemax'));

        let rcSliderMinValueAdj = 0;
        let rcSliderMaxValueAdj = rcSliderMaxValue - rcSliderMinValue;
        
        if (min < rcSliderMinValue) {
            min = rcSliderMinValue;
        }

        if (max > rcSliderMaxValue) {
            max = rcSliderMaxValue;
        }

        let minAdj = min - rcSliderMinValue;
        let maxAdj = max - rcSliderMinValue;

        let minDesiredX = Math.round((minAdj / rcSliderMaxValueAdj) * rcSliderWidth);
        let maxDesiredX = Math.round((maxAdj / rcSliderMaxValueAdj) * rcSliderWidth);

        rcSliderMinHandle.dragAndDrop({ x: minDesiredX , y: 0 });
        rcSliderMaxHandle.dragAndDrop({ x: maxDesiredX - rcSliderWidth, y: 0 });

        this.clickOpenFilterSaveButton();
    }


    // These filters are currently unused! 
    // Enable and test them when implemented into test cases. 

/*
    setLeaseOptionsFilter(term, mileage) {
        const termOptions = [24,36,48,60];
        if (!termOptions.includes(term)) {
            throw new Error("Lease term needs to be 24, 36, 48, or 60!");
        }
        const mileageOptions = [10000,15000,20000,25000,30000,35000,40000,45000,50000,55000,60000];
        if (!mileageOptions.includes(mileage)) {
            throw new Error("Mileage needs to be 10000-60000 in steps of 5000!");
        }

        this.filterLeaseOptionsButton.click();
        let leaseOptionsFilter = $('[data-key="LeaseOptionFilter"]');
        leaseOptionsFilter.waitForExist({ timeout: MENU_TIMEOUT });

        let rcSlider = leaseOptionsFilter.$('[data-component="DurationSlider"]').$('.rc-slider');
        let rcSliderWidth = rcSlider.getSize('width');
        let rcSliderHandle = rcSlider.$('.rc-slider-handle');

        let rcSliderMinValue = parseInt(rcSliderHandle.getAttribute('aria-valuemin'));
        let rcSliderMaxValue = parseInt(rcSliderHandle.getAttribute('aria-valuemax'));

        let rcSliderMinValueAdj = 0;
        let rcSliderMaxValueAdj = rcSliderMaxValue - rcSliderMinValue;

        let adj = term - rcSliderMinValue;
        let minDesiredX = Math.round((adj / rcSliderMaxValueAdj) * rcSliderWidth);
        rcSliderHandle.dragAndDrop({ x: minDesiredX , y: 0 });


        rcSlider = leaseOptionsFilter.$('[data-component="MileageSlider"]').$('.rc-slider');
        rcSliderWidth = rcSlider.getSize('width');
        rcSliderHandle = rcSlider.$('.rc-slider-handle');

        rcSliderMinValue = parseInt(rcSliderHandle.getAttribute('aria-valuemin'));
        rcSliderMaxValue = parseInt(rcSliderHandle.getAttribute('aria-valuemax'));

        rcSliderMinValueAdj = 0;
        rcSliderMaxValueAdj = rcSliderMaxValue - rcSliderMinValue;

        adj = mileage - rcSliderMinValue;
        minDesiredX = Math.round((adj / rcSliderMaxValueAdj) * rcSliderWidth);
        rcSliderHandle.dragAndDrop({ x: minDesiredX , y: 0 });

        this.clickOpenFilterSaveButton();
    }

    setFuelTypeFilter (fuelType) {
        this.filterFuelTypeButton.click();
        let fuelTypeFilterMenu = $('[data-key="fuelTypes"]').$('[data-component="CheckboxFilter"]');
        fuelTypeFilterMenu.waitForExist({ timeout: MENU_TIMEOUT });
        fuelTypeFilterMenu.$(`[input id="fuelType-${fuelType}"]`).$('..').click();

        this.clickOpenFilterSaveButton();
    }

    setBodyTypeFilter (bodyType) {
        this.filterBodyTypeButton.click();
        let bodyTypeFilterMenu = $('[data-key="bodyTypes"]').$('[data-component="CheckboxFilter"]');
        bodyTypeFilterMenu.waitForExist({ timeout: MENU_TIMEOUT });
        bodyTypeFilterMenu.$(`[input id="bodyType-${bodyType}"]`).$('..').click();

        this.clickOpenFilterSaveButton();
    }

    setTransmissionFilter (transmissionType) {
        this.filterTransmissionButton.click();
        let bodyTransmissionMenu = $('[data-key="transmissions"]').$('[data-component="CheckboxFilter"]');
        bodyTransmissionMenu.waitForExist({ timeout: MENU_TIMEOUT });
        bodyTransmissionMenu.$(`[input id="transmission-${transmissionType}"]`).$('..').click();

        this.clickOpenFilterSaveButton();
    }

*/

    /* Primary search bar for makes */

    get vehicleMakeSearchBarInput () { return $('[data-component="VehicleSearch"]').$('input') }
    get vehicleMakeSearchBarButton () { return $('[data-component="VehicleSearch"]').$('button') }
    get vehicleMakeSearchError () { return $('[data-key="features.showroom.search.noresults"]') }

    /* Secondary search bar for makes and models */
    get makeModelSearchBarInput () { 
        let makeModelFilterMenu = $('[data-component="MakeModelFilter"]');
        makeModelFilterMenu.waitForExist({ timeout: MENU_TIMEOUT });
        return makeModelFilterMenu.$("..").$("..").$('[class^="SearchBarWrapper"]').$("input");
    }

    makeModelFilterMenu () { 
        return $('[data-component="MakeModelFilter"]');
    }

    /* Offers */

    // Each offer has the component id: data-component="VehicleCard"
    get carOfferings () { return $('[data-tag-id="component-filtered-cars-grid"]') }
    get totalOfferCount () { return this.carOfferings.$$('[data-component="VehicleCard"]').length }
    get allOffers () { return this.carOfferings.$$('[data-component="VehicleCard"]') }
    get bestDeals () { return this.carOfferings.$$('span[class^="Ribbon"][color="midOrange"]') }
    get noStressPlans () { return this.carOfferings.$$('span[class^="Ribbon"][color="aquaBlue"]') }

    get sortByInput () { return $('[data-tag-id="sort-by"]').$('#orderBy_input') }

    makeModelOffersCount (make, model) { 
        let count = 0;
        let offers = this.carOfferings.$$('[data-component="VehicleDescription"]');
        for (let i = 0; i<offers.length; i++) {
            console.log(`loop: ${i}, make: ${make}, model: ${model}, offers length: ${offers.length}, text: ${offers[i].getText()}`);
            if (offers[i].getText().toLowerCase().indexOf(make.toLowerCase()) == -1)
                continue;
            if ((offers[i].getText().toLowerCase().indexOf(model.toLowerCase()) == -1) && (model != "[none]"))
                continue;
            count++;
        }
        return count;
    }

    get monthlyPriceMinMax () {
        this.filterMonthlyPriceButton.click();
        let priceFilter = $('[data-component="PriceFilter"]');
        priceFilter.waitForExist({ timeout: MENU_TIMEOUT });

        let rcSlider = priceFilter.$('.rc-slider');

        let rcSliderMinHandle = rcSlider.$('.rc-slider-handle-1');
        let rcSliderMaxHandle = rcSlider.$('.rc-slider-handle-2');

        let rcSliderMinValue = parseInt(rcSliderMinHandle.getAttribute('aria-valuenow'));
        let rcSliderMaxValue = parseInt(rcSliderMaxHandle.getAttribute('aria-valuenow'));

        return {
            min: rcSliderMinValue,
            max: rcSliderMaxValue
        }
    }

    /**
     * overwrite specifc options to adapt it to page object
     */
    open () {
        return super.open(`/${leasePlanLanguage}/business/showroom`);
    }

}

module.exports = new ShowroomBusinessPage();
