/**
* main page object containing all methods, selectors and functionality
* that is shared across all page objects
*/
module.exports = class Page {
    /**
    * Opens a sub page of the page
    * @param path path of the sub page (e.g. /path/to/page.html)
    */

    open (path) {
        // open the requested path
        let r = browser.url(path);
        // remove the "accept cookies" dom object
        browser.execute(() => {
            const alertBox = document.querySelector('.optanon-alert-box-wrapper');
            const alertBoxOverlay = document.getElementById('optanon');
            alertBox.remove();
            alertBoxOverlay.remove();
        });
        return r;
    }
}
