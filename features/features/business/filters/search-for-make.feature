Feature: Business Vehicle Make Search
    As a business representative browsing the website, I want to search for a make.

    Background: 
        Given I am on the business vehicles page

    Scenario Outline: I want to type a search for a specific make
        When I search for <make_search> in the make search bar
        Then the <make> filter should be applied

        Examples:
            | make_search  | make            |
            | for          | make-FORD       |
            | volk         | make-VOLKSWAGEN |
            | bmw          | make-BMW        |


    Scenario: I want to type a search for a make, but it doesn't exist
        When I search for abcdef 123456 in the make search bar
        Then display a "0 results" error