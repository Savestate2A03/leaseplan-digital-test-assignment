Feature: Business Vehicle Filtering
    As a business representative browsing the website, I want to be able to filter vehicles based on my business' needs. 

    Background: 
        Given I am on the business vehicles page

    Scenario Outline: I want to set popular filters
        When I set the <filter> popular filter
        Then I should see only <filter> results

        Examples:
            | filter             |
            | Best deals         |
            | Configure yourself |
            | No stress plan     |


    Scenario Outline: I want to search for a specific make and model
        When I filter make <make> and model <model>
        Then I should see only <make> makes and <model> models

        Examples:
            | make  | model  |
            | audi  | [none] |
            | ford  | focus  |


    Scenario Outline: I want to search for a specific make or model name
        When I search for <term> in the Make and Model menu
        Then I should see <make> in the Make and Model menu

        Examples:
            | term         | make       |
            | AU           | Audi       |
            | AU           | Renault    |
            | nIrO         | Kia        |
            | X1           | BMW        |
            | garbage_term | [none]     |


    Scenario Outline: I want to search for a range in monthly price
        When I set the monthly price sliders between <min> and <max>
        Then I should see only results in my price range

        Examples:
            | min  | max  |
            | 300  | 500  |
            | 1900 | 2100 |


    Scenario: I want to clear filters
        When I set a filter
        And I click the delete filters button
        Then all filters should be removed


    Scenario: I want to sort the list of vehicles
        When I set a filter
        And I sort from price low to high
        Then the lowest prices are displayed first
        When I sort from price high to low
        Then the highest prices are displayed first


# No simple method of ensuring valid results for these at the moment. 
# Information from the business would help me be able to define
# a successful test case or not.

# First idea would be to check the predicted results number to the
# actual listed number, ex: checking "Electric (46)" then checking
# the number of results from data-key=features.showroom.toChooseFrom
# but again, I'm not going to implement this unless I had more
# information. 

#    Scenario: I want to search for a specific mileage and term
#        When I set the term and mileage sliders 
#        Then I should see only matching results 
#
#
#    Scenario: I want to search for a specific fuel type
#        When I set a fuel type filter
#        Then I should see only matching results
#
#
#    Scenario: I want to search for a body type
#        When I set a body type filter
#        Then I should see only matching results
#
#
#    Scenario: I want to search for a transmission type
#        When I set a transmission filter
#        Then I should see only matching results
#
#
#    Scenario: I want to remove all search filters
#        When I click the trash icon
#        Then all filters should be removed
#