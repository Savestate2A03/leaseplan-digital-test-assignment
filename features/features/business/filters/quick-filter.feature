Feature: Business Vehicle Quick Filters
    As a business representative browsing the website, I want to quickly filter vehicles using suggested filters.

    Background:
        Given I am on the business vehicles page

    Scenario Outline: I want to use a quick filter
        When I click on the quick filter <quick_filter>
        Then the <applied_filter> filter should be applied

    Examples:
        | quick_filter       | applied_filter         |
        | Electric           | fuelType-Electric      |
        | SUV                | bodyType-SUV           |
        | Automatic          | transmission-Automatic |
        | Hybrid             | fuelType-Hybrid        |
        | Petrol             | fuelType-Petrol        |