# LeasePlan Digital
Test Assignment Setup by Joseph El-Khouri
## Overview
This document covers the setup required to run and create new tests for the test assignment for the available QA position. 
## Machine Setup
Before you can write and execute test cases, you will need to set up your machine with all the relevant test development software and configure it as a runner in the GitLab project. This setup assumes a Windows 10 installation, although it can likely be adapted for other operating systems without too much trouble.

**Prerequisites (install before continuing)**
* **Node.js** (latest LTS version, 14.16.0 as of documentation)  
  _Install with “Additional Tools” option checked to ensure completeness with installation_  
  https://nodejs.org/en/ 
* **Python v3+** (latest version, 3.9.2 as of documentation)  
  _Make sure to check “Add Python 3.x to PATH” during installation_  
  https://www.python.org/downloads/
* **Text editor**. I’ve listed some popular examples:
  * Visual Studio Code: https://code.visualstudio.com 
  * Sublime Text: https://www.sublimetext.com/ 
  * Notepad++: https://notepad-plus-plus.org/downloads/ 
* **Git** (latest version, 2.30.1 as of documentation)  
  https://git-scm.com/downloads
* **Google Chrome**  
  https://www.google.com/chrome/ 
## Project Setup
After installing the required prerequisites, you will need to clone the repository to your machine. 

Run the following command on your local machine in the _Git Bash_ in a directory you would like to use as your working directory:
```
git clone https://gitlab.com/Savestate2A03/leaseplan-digital-test-assignment
```
Next, change directory into the repository with `cd leaseplan-digital-test-assignment` and run `npm install` to install the required Node.js modules.
## Running the test suite
To run the currently existing test suite of features, run the following command:
```
npx wdio run ./wdio.conf.js
```
WebdriverIO will kick off running the test suite using _chromedriver_. If you would like to watch the testing suite manipulate the webpage in realtime, remove the `'--headless'` option from `goog:chromeOptions` in _wdio.conf.js_. 
## Reporting
The current GitLab repository is configured to publish test reports that are generated with `@rpii/wdio-html-reporter`. Locally, you can find the reports under `./reports/html-reports/`, but these also get published as artifacts to each build, and to the [public pages](https://savestate2a03.gitlab.io/leaseplan-digital-test-assignment/html-reports/master-report.html) site on completion of each build. 

As a side note, it looks like this HTML reporter is a bit funky with its formatting. If I had more time for this assignment, I'd switch to one that generated more coherent reports. As for now though, at least the framework is in place to do so if needed.
## Writing test cases
To write test cases for this project, create your appopritate Page Object if needed in `./features/pageobjects`, your step definitions in `./features/step-definitions/steps.js`, and organize your feature files appopriately in `./features/features/**`.

Feature files are written for [Cucumber in the Gherkin syntax](https://cucumber.io/docs/gherkin/reference/), while Page Object models and step definitions are written in JavaScript. 
## GitLab CI
Currently, GitLab CI is set up as a shell on a Windows development machine. The pipeline information can be found in the `.gitlab-ci.yml` file. Currently, it contains two job functions: `test`, and `deploy`. 

`test` installs the node modules required to run the project, and then runs the test suite

`deploy` hands the reports off to GitLab for publishing. 
